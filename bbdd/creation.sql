DROP DATABASE IF EXISTS PruebasCorona;

CREATE DATABASE PruebasCorona CHARACTER SET latin1;

use PruebasCorona

CREATE TABLE AllData(
	
	fecha DATE,
	localidad VARCHAR(30),
	test_rapidos_total INT,
	test_rapidos_positivo INT,
	pcr_total INT,
	pcr_positivos INT,
	codigo_ine VARCHAR(30),
	total_pruebas INT,
	total_pruebas_positivas INT,
	test_rapidos_porcentaje DECIMAL(17,15),
	pcr_porcentaje DECIMAL(17,15),
	porcentaje_test_total DECIMAL(17,15),
	sigla CHAR(3),
	ID INT AUTO_INCREMENT,
	PRIMARY KEY (ID)


);

CREATE TABLE Avila(
	
	fecha DATE,
	localidad VARCHAR(30),
	test_rapidos_total INT,
	test_rapidos_positivo INT,
	pcr_total INT,
	pcr_positivos INT,
	codigo_ine VARCHAR(30),
	total_pruebas INT,
	total_pruebas_positivas INT,
	test_rapidos_porcentaje DECIMAL(17,15),
	pcr_porcentaje DECIMAL(17,15),
	porcentaje_test_total DECIMAL(17,15),
	sigla CHAR(3),
	ID INT AUTO_INCREMENT,
	ID_ALLDATA INT,
	PRIMARY KEY (ID),
	CONSTRAINT Avila_AllData FOREIGN KEY (ID_ALLDATA) REFERENCES AllData (ID)

);
CREATE TABLE Burgos(


	fecha DATE,
	localidad VARCHAR(30),
	test_rapidos_total INT,
	test_rapidos_positivo INT,
	pcr_total INT,
	pcr_positivos INT,
	codigo_ine VARCHAR(30),
	total_pruebas INT,
	total_pruebas_positivas INT,
	test_rapidos_porcentaje DECIMAL(17,15),
	pcr_porcentaje DECIMAL(17,15),
	porcentaje_test_total DECIMAL(17,15),
	sigla CHAR(3),
	ID INT AUTO_INCREMENT,
	ID_AVILA INT ,
	PRIMARY KEY (ID),
	CONSTRAINT Burgos_Avila FOREIGN KEY (ID_AVILA ) REFERENCES Avila (ID)

);
CREATE TABLE Leon(

	fecha DATE,
	localidad VARCHAR(30),
	test_rapidos_total INT,
	test_rapidos_positivo INT,
	pcr_total INT,
	pcr_positivos INT,
	codigo_ine VARCHAR(30),
	total_pruebas INT,
	total_pruebas_positivas INT,
	test_rapidos_porcentaje DECIMAL(17,15),
	pcr_porcentaje DECIMAL(17,15),
	porcentaje_test_total DECIMAL(17,15),
	sigla CHAR(3),
	ID INT AUTO_INCREMENT,
	ID_BURGOS INT,
	PRIMARY KEY (ID),
	CONSTRAINT Leon_Burgos FOREIGN KEY (ID_BURGOS) REFERENCES Burgos (ID)

);
CREATE TABLE Palencia(

	fecha DATE,
	localidad VARCHAR(30),
	test_rapidos_total INT,
	test_rapidos_positivo INT,
	pcr_total INT,
	pcr_positivos INT,
	codigo_ine VARCHAR(30),
	total_pruebas INT,
	total_pruebas_positivas INT,
	test_rapidos_porcentaje DECIMAL(17,15),
	pcr_porcentaje DECIMAL(17,15),
	porcentaje_test_total DECIMAL(17,15),
	sigla CHAR(3),
	ID INT AUTO_INCREMENT,
	ID_LEON INT,
	PRIMARY KEY (ID),
	CONSTRAINT Palencia_Leon FOREIGN KEY (ID_LEON ) REFERENCES Leon (ID)

);
CREATE TABLE Salamanca(

	fecha DATE,
	localidad VARCHAR(30),
	test_rapidos_total INT,
	test_rapidos_positivo INT,
	pcr_total INT,
	pcr_positivos INT,
	codigo_ine VARCHAR(30),
	total_pruebas INT,
	total_pruebas_positivas INT,
	test_rapidos_porcentaje DECIMAL(17,15),
	pcr_porcentaje DECIMAL(17,15),
	porcentaje_test_total DECIMAL(17,15),
	sigla CHAR(3),
	ID INT AUTO_INCREMENT,
	ID_PALENCIA INT,
	PRIMARY KEY (ID),
	CONSTRAINT Salamanca_Palencia FOREIGN KEY (ID_PALENCIA) REFERENCES Palencia (ID)


);
CREATE TABLE Segovia(

	fecha DATE,
	localidad VARCHAR(30),
	test_rapidos_total INT,
	test_rapidos_positivo INT,
	pcr_total INT,
	pcr_positivos INT,
	codigo_ine VARCHAR(30),
	total_pruebas INT,
	total_pruebas_positivas INT,
	test_rapidos_porcentaje DECIMAL(17,15),
	pcr_porcentaje DECIMAL(17,15),
	porcentaje_test_total DECIMAL(17,15),
	sigla CHAR(3),
	ID INT AUTO_INCREMENT,
	ID_SALAMANCA INT,
	PRIMARY KEY (ID),
	CONSTRAINT Segovia_Salamanca FOREIGN KEY (ID_SALAMANCA) REFERENCES Salamanca (ID)

);
CREATE TABLE Soria(

	fecha DATE,
	localidad VARCHAR(30),
	test_rapidos_total INT,
	test_rapidos_positivo INT,
	pcr_total INT,
	pcr_positivos INT,
	codigo_ine VARCHAR(30),
	total_pruebas INT,
	total_pruebas_positivas INT,
	test_rapidos_porcentaje DECIMAL(17,15),
	pcr_porcentaje DECIMAL(17,15),
	porcentaje_test_total DECIMAL(17,15),
	sigla CHAR(3),
	ID INT AUTO_INCREMENT,
	ID_SEGOVIA INT,
	PRIMARY KEY (ID),
	CONSTRAINT Soria_Segovia FOREIGN KEY (ID_SEGOVIA) REFERENCES Segovia (ID)


);
CREATE TABLE Valladolid(

	fecha DATE,
	localidad VARCHAR(30),
	test_rapidos_total INT,
	test_rapidos_positivo INT,
	pcr_total INT,
	pcr_positivos INT,
	codigo_ine VARCHAR(30),
	total_pruebas INT,
	total_pruebas_positivas INT,
	test_rapidos_porcentaje DECIMAL(17,15),
	pcr_porcentaje DECIMAL(17,15),
	porcentaje_test_total DECIMAL(17,15),
	sigla CHAR(3),
	ID INT AUTO_INCREMENT,
	ID_SORIA INT, 
	PRIMARY KEY (ID),
	CONSTRAINT Valladolid_Soria FOREIGN KEY (ID_SORIA) REFERENCES Soria (ID)


);
CREATE TABLE Zamora(

	fecha DATE,
	localidad VARCHAR(30),
	test_rapidos_total INT,
	test_rapidos_positivo INT,
	pcr_total INT,
	pcr_positivos INT,
	codigo_ine VARCHAR(30),
	total_pruebas INT,
	total_pruebas_positivas INT,
	test_rapidos_porcentaje DECIMAL(17,15),
	pcr_porcentaje DECIMAL(17,15),
	porcentaje_test_total DECIMAL(17,15),
	sigla CHAR(3),
	ID INT AUTO_INCREMENT,
	ID_VALLADOLID INT,
	PRIMARY KEY (ID),
	CONSTRAINT Zamora_Valladolid FOREIGN KEY (ID_VALLADOLID) REFERENCES Valladolid (ID)


);

LOAD DATA 
LOCAL INFILE 'AllData.csv'
INTO TABLE AllData
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n';

LOAD DATA 
LOCAL INFILE 'Avila.csv'
INTO TABLE Avila
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n';

LOAD DATA 
LOCAL INFILE 'Burgos.csv'
INTO TABLE Burgos
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n';
 
LOAD DATA 
LOCAL INFILE 'Leon.csv'
INTO TABLE Leon
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n';

LOAD DATA 
LOCAL INFILE 'Palencia.csv'
INTO TABLE Palencia
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n';

LOAD DATA 
LOCAL INFILE 'Salamanca.csv'
INTO TABLE Salamanca
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n';

LOAD DATA 
LOCAL INFILE 'Segovia.csv'
INTO TABLE Segovia
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n';

LOAD DATA 
LOCAL INFILE 'Soria.csv'
INTO TABLE Soria
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n';

LOAD DATA 
LOCAL INFILE 'Valladolid.csv'
INTO TABLE Valladolid
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n';

LOAD DATA 
LOCAL INFILE 'Zamora.csv'
INTO TABLE Zamora
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n';
