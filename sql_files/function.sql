DROP FUNCTION IF EXISTS  num_contagios;

SELECT 'Esta Función Muestra el Número de contagios de data una Fecha en toda la Comunidad \n| 1º Parametro --> Fecha deseada' AS Funcionamiento;
SELECT 'Del 2020-03-13 al 2020-03-27 \n| Del 2020-04-04 al 2020-04-30 \n| Del 2020-05-01 al 2020-05-27' AS 'Franjas Posibles de Fechas';
SELECT 'Para llamar a la Función --> SELECT num_contagios (fecha);' AS Sintaxis;

DELIMITER //
CREATE FUNCTION num_contagios(fecha_dada DATE)
RETURNS INT
BEGIN
	
DECLARE Contagios INT DEFAULT 0;
DECLARE STOP INT DEFAULT 1;
DECLARE IDC INT;

-- Guardamos en IDC el primer ID asociado a cada fecha
SET IDC = (SELECT ID FROM AllData WHERE  fecha = fecha_dada LIMIT 1);

WHILE STOP = 1 DO

-- Mientras concuerde el ID con la fecha dada COUNT devolvera 1, sino 0, finalizando asi el bucle y la función
IF (SELECT count(*) FROM AllData WHERE fecha = fecha_dada AND ID = IDC) = 0 
THEN 
SET STOP = 0;
RETURN Contagios;
END IF;

-- En Contagios guardamos el resultado de la consulta que nos devulve el numero de infectados recorriendo los IDs de la fecha
SET Contagios = Contagios + (SELECT total_pruebas_positivas FROM AllData WHERE fecha = fecha_dada AND ID = IDC);
SET IDC = IDC+1;
							
END WHILE;
END //

DELIMITER ;
