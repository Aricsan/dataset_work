-- Que porcentaje de Positivos hay de todas las Pruebas
SELECT fecha AS Dia, SUM(total_pruebas) AS 'Total Pruebas en la Comunidad', SUM(total_pruebas_positivas) AS 'Total Pruebas Positivas', CONCAT((SUM(total_pruebas_positivas) * 100) / SUM(total_pruebas), '%') AS 'Porcetaje de Positivos' FROM AllData GROUP BY fecha;

-- Dias con mas y menos positivos
SELECT DISTINCT fecha AS Dia, total_pruebas_positivas AS 'Maximo de Pruebas Positivas' FROM AllData WHERE total_pruebas_positivas = (SELECT MAX(total_pruebas_positivas) FROM AllData) OR total_pruebas_positivas = (SELECT MIN(total_pruebas_positivas) FROM AllData) ORDER BY fecha DESC;


-- Pruebas totales en toda la Comunidad por Provincias
SELECT localidad AS Provincia, SUM(test_rapidos_total) + SUM(pcr_total) AS 'Total de Tests' FROM AllData GROUP BY localidad;

-- Media de Infectados por Coronavirus, registro Diario
SELECT fecha AS DIA, ROUND(AVG(total_pruebas_positivas)) AS 'Media Infectados' FROM AllData GROUP BY fecha;

-- Mostrar el total de positivos de Burgos y León en una semana Registro Diario
SELECT Burgos.fecha, SUM(Leon.total_pruebas_positivas) AS 'Positivos Leon', SUM(Burgos.total_pruebas_positivas) AS 'Positivos Burgos' FROM Burgos JOIN Leon WHERE Burgos.ID = Leon.ID GROUP BY Burgos.fecha;
