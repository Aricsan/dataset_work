DROP PROCEDURE IF EXISTS analisis_provincias;

SELECT 'Este Procedimiento muestra Información sobre la Provincia indicada \n| 1º Parametro --> Opcion de Analisis \n| 2º Parametro --> Nombre de la Provincia' AS Funcionamiento;
SELECT '1 --> Media Infectados \n| 2 --> Total de Pruebas \n| 3 --> Media de Efectividad de los Test PCR \n| 4 --> Muestra las tres Opciones Juntas' AS 'Posibles Opciones';
SELECT 'Para llamar al Procedimiento --> CALL analisis_provincias (opcion, provincia);' AS Sintaxis;

DELIMITER **
CREATE PROCEDURE analisis_provincias (IN Opcion INT, IN Provincia VARCHAR(10))

BEGIN

CASE Opcion

WHEN 1 THEN
SELECT ROUND(AVG(total_pruebas_positivas)) AS 'Media Infectados' FROM AllData WHERE localidad = Provincia;

WHEN 2 THEN 
SELECT SUM(test_rapidos_total) + SUM(pcr_total) AS 'Total de Pruebas' FROM AllData Where localidad = Provincia;

WHEN 3 THEN 
SELECT CONCAT(ROUND(AVG(pcr_porcentaje),2), '%') AS 'Media de Efectividad de los Test PCR' FROM AllData WHERE localidad = Provincia;

WHEN 4 THEN
SELECT ROUND(AVG(total_pruebas_positivas)) AS 'Media Infectados', SUM(test_rapidos_total) + SUM(pcr_total) AS 'Total de Pruebas', CONCAT(ROUND(AVG(pcr_porcentaje),2), '%') AS 'Media de Efectividad de los Test PCR' FROM AllData WHERE localidad = Provincia;

ELSE
SELECT 'Parametros Erroneos';

END CASE;

END **
DELIMITER ;
