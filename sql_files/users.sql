-- Eliminación de Usuarios (si existen)
DROP USER IF EXISTS 'Estadistica'@'%';
DROP USER IF EXISTS 'Admin'@'localhost';
DROP USER IF EXISTS 'INE'@'localhost';
DROP USER IF EXISTS 'Invitado'@'%';
DROP USER IF EXISTS 'PresidenteComunidad'@'localhost';

-- Creación de Usuarios
CREATE USER 'Estadistica'@'%' IDENTIFIED BY 'Estad1st1ca@960' PASSWORD EXPIRE INTERVAL 30 DAY;
CREATE USER 'Admin'@'localhost' IDENTIFIED BY 'Adm1n1istrad0r@152' PASSWORD EXPIRE INTERVAL 30 DAY;
CREATE USER 'INE'@'localhost' IDENTIFIED BY 'Inst1tut0Nac1on4lEstad1st1c4@173' PASSWORD EXPIRE INTERVAL 30 DAY;
CREATE USER 'Invitado'@'%' IDENTIFIED BY 'Inv1tad0@016';
CREATE USER 'PresidenteComunidad'@'localhost' IDENTIFIED BY 'Pres1d3nt3C0m6n1d4ad@614' PASSWORD EXPIRE INTERVAL 30 DAY;

-- Distribucion de Permisos
GRANT SELECT ON PruebasCorona.* TO 'Estadistica'@'%';
GRANT ALL ON PruebasCorona.* TO 'Admin'@'localhost';
GRANT INSERT, UPDATE, DELETE, CREATE ON PruebasCorona.* TO 'INE'@'localhost';
GRANT SELECT ON PruebasCorona.AllData TO 'Invitado'@'%';
GRANT SELECT, CREATE VIEW, SHOW VIEW ON PruebasCorona.* TO 'PresidenteComunidad'@'localhost';
