# Dataset
- The Dataset is the collection of data about the Coronavirus in the province of Castilla y León (Spain) during **13/03/2020 - 27/05/2020**  
- [Download link](https://datos.gob.es/es/catalogo/a07002862-pruebas-realizadas-coronavirus1)  

# Instructions (Terminal)
- To create and import the Database you must start MySQL, and load the creation file located in the directory **bbdd** with the command **SOURCE creation.sql;**        
- To use the Functions, Procedures and Queries, the want file must be loaded into MySQL from the **sql_files** directory with the command **SOURCE file.sql**;  
- It is obligatory to enjoy ;)
